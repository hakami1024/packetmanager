# 1) UNION: all users, who have Windows or installed VirtualBox

DROP VIEW IF EXISTS WindowsUsersIds;
CREATE VIEW WindowsUsersIds AS 
SELECT userId FROM Hosts 
	INNER JOIN OS ON OS.osId=Hosts.osId AND OS.osPlatform='windows'; 
    
DROP VIEW IF EXISTS VirtualboxAppId;
CREATE VIEW VirtualboxAppId AS
SELECT Apps.appId FROM Apps WHERE Apps.appName = 'virtualbox' LIMIT 1;
                 
SELECT login FROM Users WHERE userId IN (SELECT * FROM WindowsUsersIds)
UNION
SELECT login FROM Users WHERE userId IN (
	SELECT Hosts.userId FROM Hosts WHERE 
		EXISTS( SELECT * FROM VirtualboxAppId)
        AND app_installed((SELECT * FROM VirtualboxAppId LIMIT 1), Hosts.hostId)
	);
    
# 2) INTERSECTION: all linuxoids, who installed JDK

DROP VIEW IF EXISTS JdkId;
CREATE VIEW JdkId AS
SELECT Apps.appId FROM Apps WHERE Apps.appName = 'JDK' LIMIT 1;

SELECT login FROM Users 
WHERE EXISTS(
	SELECT * FROM Hosts INNER JOIN OS 
    ON Hosts.osId = OS.osId 
	   AND OS.osPlatform = 'linux'
       AND EXISTS(SELECT * FROM JdkId)
	   AND app_installed((SELECT * FROM JdkId LIMIT 1),
						Hosts.hostId)
	WHERE Hosts.userId = Users.userId);
	

# 3) INTERSECTION: all transactions related to git packet and hakami's Arch

SELECT * FROM Transactions 
WHERE Transactions.transId IN (
	SELECT transId FROM TransHost
		INNER JOIN Hosts
        ON TransHost.hostId = Hosts.hostId
        AND Hosts.hostName = 'Hakami\'s Arch'
	)
    AND Transactions.transId IN (
			SELECT transId FROM TransVer
            INNER JOIN (Versions 
						INNER JOIN Packets
                        ON Versions.packetId = Packets.packetId
                        AND Packets.packetName = 'git')
			ON TransVer.versionId = Versions.versionId
    );

# 4) MINUS: all packets necessary for git, that exitsts on Arch repository
# and not installed on hakami's Arch

DROP VIEW IF EXISTS ArchRepos;
CREATE VIEW ArchRepos AS
SELECT * FROM Repos 
WHERE Repos.osId IN (SELECT OS.osId FROM OS 
					 WHERE OS.osName = 'Arch Linux');

DROP VIEW IF EXISTS HakamiArch;                     
CREATE VIEW HakamiArch AS
SELECT * FROM Hosts WHERE Hosts.hostName = 'Hakami\'s Arch';

SELECT * FROM Packets 
WHERE NOT Packets.packetId IN (
	SELECT Versions.packetId FROM Versions
		INNER JOIN VerHost 
        ON Versions.versionId = VerHost.versionId
		INNER JOIN HakamiArch 
        ON VerHost.hostId = HakamiArch.hostId)
    AND Packets.packetId IN (
		SELECT Versions.packetId FROM Versions
			INNER JOIN VerRepo 
            ON Versions.versionId = VerRepo.versionId
			INNER JOIN ArchRepos
			ON VerRepo.repoId = ArchRepos.repoId); 
			

# 5) MINUS: all hosts, that have JDK, but never updated it

SELECT * FROM Hosts
WHERE app_installed((SELECT * FROM JdkId LIMIT 1), Hosts.hostId)
	AND app_last_update((SELECT * FROM JdkId LIMIT 1), Hosts.hostId) IS NULL;

# 6) TIMES: all version sets, suitable for app with name, containing "JDK", for all OS

DROP VIEW IF EXISTS JdkApps;
CREATE VIEW JdkApps AS
SELECT appId FROM Apps WHERE appName LIKE '%JDK%';

SELECT * FROM Versions, OS
WHERE EXISTS(
	SELECT * FROM Versions 
    INNER JOIN Packets
    ON Packets.packetId = Versions.packetId
    INNER JOIN PacApp
    ON Packets.packetId = PacApp.packetId
		AND PacApp.appId IN (SELECT * FROM JdkApps)
    INNER JOIN VerRepo
    ON Versions.versionId = VerRepo.versionId
    INNER JOIN Repos
    ON VerRepo.repoId = Repos.repoId 
    INNER JOIN OS
	ON Repos.repoId = OS.osId
    );



