import mysql.connector
from mysql.connector import Error
import requests
import re
import json
import random

__author__ = 'hakami'


# payload = {'login': 'just4fun', 'password': '666'}
# url = 'https://httpbin.org/post'
# #url = "http://uhkk8caae591.hakami1024.koding.io:8000/hello/"
# url = "http://uhkk8caae591.hakami1024.koding.io:8000/signup/"
# r = requests.post(url, data=json.dumps(payload))
# print(r.text)
# raise SystemExit

# p = re.compile(r'\<a href="[^"]*"\>[^\<]*')

mainUrl = "http://mirror.yandex.ru/archlinux/"
conn1 = mysql.connector.connect(host='localhost',
                                       database='PacketManager',
                                       user='root',
                                       password='')

conn2 = mysql.connector.connect(host='localhost',
                                       database='Warehouse',
                                       user='root',
                                       password='')

def copy():
    try:
        cursor1 = conn1.cursor()
        cursor2 = conn2.cursor()

        cursor1.execute("SELECT * FROM Apps")
        for data in cursor1:
            print(data)
            d = (data[0], data[1], 'NULL' if data[2] is None else data[2])
            print(d)

            cursor2.execute("INSERT INTO Apps VALUES " + str(d))

        conn2.commit()
    except Error as e:
        print(e)
    finally:
        conn1.close()
        conn2.close()

def installation():
    try:
        cursor2 = conn2.cursor()

        cursor2.execute("SELECT * FROM Apps")
        apps = [app for app in cursor2]

        cursor2.execute("SELECT * FROM Transactions")
        transactions = [data for data in cursor2]

        cursor2.execute("SELECT * FROM Users")
        users = [data for data in cursor2]

        cursor2.execute("SELECT * FROM Hosts")
        hosts = [data for data in cursor2]

        cursor2.execute("SELECT * FROM Repo")
        repo = [data for data in cursor2]

        cursor2.execute("SELECT * FROM OS")
        os = [data for data in cursor2]

        i = 10
        for trans in transactions:
            print((trans[0], apps[0][0], trans[0], users[0][0], hosts[0][0], repo[0][0], os[0][0], random.random()))
            cursor2.execute("INSERT INTO Installation VALUES "
                            + str( (trans[0]+i, trans[0], users[0][0], hosts[0][0], repo[0][0], apps[0][0], 86, int(random.random()*100000))))
            i+=1
        conn2.commit()
    except Error as e:
        print(e)
    finally:
        conn2.close()


def setOS():
    try:
        cursor = conn2.cursor()
        cursor.execute("INSERT INTO OS (osName, osVersion, osPlatform) VALUES "
                       + "('OS X', '10.0', 'mac'),"
                       + "('FreeDOS', '1.1', 'dos'),"
                       + "('Android Marshmallow', '6.0', 'android'),"
                       + "('Android Lollipop', '6.0', 'android'),"
                       + "('Windows 7', '7', 'nt'),"
                       + "('Windows 8', '8', 'nt'),"
                       + "('Windows 10', '10', 'nt'),"
                       + "('FreeBSD', '11.0', 'bsd'),"
                       + "('Fedora', '24', 'linux'),"
                       + "('Gentoo', '2008.0', 'linux'),"
                       + "('Linux Mint', '17.3', 'linux'),"
                       + "('Ubuntu', '15.10', 'linux'),"
                       + "('Kali Linux', '2.0', 'linux'),"
                       + "('Slackware', '14.1', 'linux')")
        conn2.commit()
        # row = cursor.fetchone()
        #
        # while row is not None:
        #     print(row)
        #     row = cursor.fetchone()
        #
        # if conn.is_connected():
        #     print('Connected to MySQL database')
    except Error as e:
        print(e)
    finally:
        conn2.close()

#setOS()
#copy()
installation()

raise SystemExit

def crawl(url):
    print("on ", url)
    try:
        r = requests.get(url)
    except Error as e:
        print(e)
        return

    iterator = p.finditer(r.text)

    results = []
    for match in iterator:
        str = match.group()
        results.append(str[str.index('>')+1:])

    for result in results:
        if result[-1] == '/' and result != "../":
            crawl(url + result)
            continue
        m = re.search(r'.*\.tar\.[A-Za-z0-9]+\Z', result)
        if m is not None:
            # print(result)
            part = re.search(r'\A[A-Za-z]+-[A-Za-z]+[^-]', result)
            if part is not None:
                processJson(part.group(0), url.replace(mainUrl, "", 1)+result)


def setApp(pkgBase, pkgDesc):
    try:
        cursor = conn1.cursor()
        cursor.execute("INSERT IGNORE INTO Apps (appName, appDescription) VALUES "
                       + "('"+pkgBase+"', '"+pkgDesc+"')")
        conn1.commit()
    except Error as e:
        print(e)

def setPacket(pkgName, pkgDesc):
    try:
        cursor = conn1.cursor()
        cursor.execute("INSERT IGNORE INTO Packets (packetName, packetDescription) VALUES "
                       + "('"+pkgName+"', '"+pkgDesc+"')")
        conn1.commit()

        cursor = conn1.cursor()
        cursor.execute("SELECT packetId FROM Packets WHERE packetName = %s AND packetDescription = %s LIMIT 1", (pkgName, pkgDesc))
        for id in cursor:
            print('packetId = '+str(id[0]))
            return id[0]
    except Error as e:
        print(e)


def setVer(pkgId, pkgVer):
    try:
        cursor = conn1.cursor()
        cursor.execute("INSERT IGNORE INTO Versions (version, packetId) VALUES "
                       + "('"+pkgVer+"', "+str(pkgId)+")")
        conn1.commit()

        cursor = conn1.cursor()
        cursor.execute("SELECT versionId FROM Versions WHERE packetId = %s AND version = %s LIMIT 1", (pkgId, pkgVer))
        for id in cursor:
            print('versionId = ' + str(id))
            return id[0]
    except Error as e:
        print(e)

def setVerRepo(verId, verUrl):
    try:
        cursor = conn1.cursor()
        cursor.execute("INSERT IGNORE INTO VerRepo (repoId, versionId, verUrl) VALUES "
                       + "(1, "+str(verId)+", '"+verUrl+"')")
        conn1.commit()
    except Error as e:
        print(e)


def processJson(verInfo, verUrl):
    print (verUrl)
    r = requests.get('https://www.archlinux.org/packages/search/json/?q='+verInfo)
    print( 'https://www.archlinux.org/packages/search/json/?q='+verInfo)
    jsonAnsw = r.json()
    print(jsonAnsw)

    if jsonAnsw['valid']:
        jsonResult = jsonAnsw['results'][0]
        pkgName = jsonResult['pkgname']
        pkgBase = jsonResult['pkgbase']
        pkgVer = jsonResult['pkgver']
        pkgDesc = jsonResult['pkgdesc']

        # if pkgBase == pkgName:
        #     setApp(pkgBase, pkgDesc)
        pkgId = setPacket(pkgName, pkgDesc)
        verId = setVer(pkgId, pkgVer)
        setVerRepo(verId, verUrl)
        print(verId)


def setOS():
    try:
        cursor = conn2.cursor()
        cursor.execute("INSERT INTO OS (osName, osVersion, osPlatform) VALUES "
                       + "('OS X', '10.0', 'mac'),"
                       + "('FreeDOS', '1.1', 'dos'),"
                       + "('Android Marshmallow', '6.0', 'android'),"
                       + "('Android Lollipop', '6.0', 'android'),"
                       + "('Windows 7', '7', 'nt'),"
                       + "('Windows 8', '8', 'nt'),"
                       + "('Windows 10', '10', 'nt'),"
                       + "('FreeBSD', '11.0', 'bsd'),"
                       + "('Fedora', '24', 'linux'),"
                       + "('Gentoo', '2008.0', 'linux'),"
                       + "('Linux Mint', '17.3', 'linux'),"
                       + "('Ubuntu', '15.10', 'linux'),"
                       + "('Kali Linux', '2.0', 'linux'),"
                       + "('Slackware', '14.1', 'linux')")
        conn1.commit()
        # row = cursor.fetchone()
        #
        # while row is not None:
        #     print(row)
        #     row = cursor.fetchone()
        #
        # if conn.is_connected():
        #     print('Connected to MySQL database')
    except Error as e:
        print(e)
    finally:
        conn1.close()

# setOS()

arch_repos = ['community/os/', 'extra/os/', 'multilib/os/', 'testing/os/']
#for repo in arch_repos:
#    crawl(mainUrl+repo)
#addToDB()
conn1.close()

