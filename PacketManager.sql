-- MySQL dump 10.15  Distrib 10.0.21-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: PacketManager
-- ------------------------------------------------------
-- Server version	10.0.21-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Apps`
--

DROP TABLE IF EXISTS `Apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Apps` (
  `appId` int(11) NOT NULL AUTO_INCREMENT,
  `appName` varchar(100) NOT NULL,
  `appDescription` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`appId`),
  UNIQUE KEY `nameApp_UNIQUE` (`appName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Apps`
--

LOCK TABLES `Apps` WRITE;
/*!40000 ALTER TABLE `Apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `Apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Hosts`
--

DROP TABLE IF EXISTS `Hosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hosts` (
  `hostId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `hostName` varchar(100) DEFAULT NULL,
  `osName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`hostId`),
  UNIQUE KEY `hostName_UNIQUE` (`hostName`),
  KEY `userId_idx` (`userId`),
  CONSTRAINT `fk_hosts_userId` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Hosts`
--

LOCK TABLES `Hosts` WRITE;
/*!40000 ALTER TABLE `Hosts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Hosts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PacApp`
--

DROP TABLE IF EXISTS `PacApp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PacApp` (
  `packetId` int(11) NOT NULL,
  `appId` int(11) NOT NULL,
  PRIMARY KEY (`packetId`,`appId`),
  KEY `fk_PacApp_idx` (`appId`),
  CONSTRAINT `fk_pacapp_appId` FOREIGN KEY (`appId`) REFERENCES `Apps` (`appId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pacapp_packetId` FOREIGN KEY (`packetId`) REFERENCES `Packets` (`packetId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PacApp`
--

LOCK TABLES `PacApp` WRITE;
/*!40000 ALTER TABLE `PacApp` DISABLE KEYS */;
/*!40000 ALTER TABLE `PacApp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Packets`
--

DROP TABLE IF EXISTS `Packets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Packets` (
  `packetId` int(11) NOT NULL AUTO_INCREMENT,
  `packetName` varchar(100) NOT NULL,
  `packetDescription` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`packetId`),
  UNIQUE KEY `packetName_UNIQUE` (`packetName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Packets`
--

LOCK TABLES `Packets` WRITE;
/*!40000 ALTER TABLE `Packets` DISABLE KEYS */;
/*!40000 ALTER TABLE `Packets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Repos`
--

DROP TABLE IF EXISTS `Repos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Repos` (
  `repoId` int(11) NOT NULL,
  `url` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`repoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Repos`
--

LOCK TABLES `Repos` WRITE;
/*!40000 ALTER TABLE `Repos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Repos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransHost`
--

DROP TABLE IF EXISTS `TransHost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransHost` (
  `transId` int(11) NOT NULL,
  `hostId` int(11) NOT NULL,
  PRIMARY KEY (`transId`,`hostId`),
  KEY `fk_TransHost_hostId_idx` (`hostId`),
  CONSTRAINT `fk_TransHost_hostId` FOREIGN KEY (`hostId`) REFERENCES `Hosts` (`hostId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_TransHost_transId` FOREIGN KEY (`transId`) REFERENCES `Transactions` (`transId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransHost`
--

LOCK TABLES `TransHost` WRITE;
/*!40000 ALTER TABLE `TransHost` DISABLE KEYS */;
/*!40000 ALTER TABLE `TransHost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransVer`
--

DROP TABLE IF EXISTS `TransVer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransVer` (
  `transId` int(11) NOT NULL,
  `versionId` int(11) NOT NULL,
  PRIMARY KEY (`transId`),
  KEY `fk_VerTrans_transId_idx` (`transId`),
  KEY `fk_TransVer_versionId_idx` (`versionId`),
  CONSTRAINT `fk_TransVer_versionId` FOREIGN KEY (`versionId`) REFERENCES `Versions` (`versionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_VerTrans_transId` FOREIGN KEY (`transId`) REFERENCES `Transactions` (`transId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransVer`
--

LOCK TABLES `TransVer` WRITE;
/*!40000 ALTER TABLE `TransVer` DISABLE KEYS */;
/*!40000 ALTER TABLE `TransVer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Transactions`
--

DROP TABLE IF EXISTS `Transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Transactions` (
  `transId` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`transId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Transactions`
--

LOCK TABLES `Transactions` WRITE;
/*!40000 ALTER TABLE `Transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `password` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VerHost`
--

DROP TABLE IF EXISTS `VerHost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VerHost` (
  `hostId` int(11) NOT NULL,
  `versionId` int(11) NOT NULL,
  `path` varchar(500) DEFAULT NULL,
  `installationTime` datetime DEFAULT NULL,
  PRIMARY KEY (`hostId`),
  KEY `fk_VerHost_versionId_idx` (`versionId`),
  CONSTRAINT `fk_VerHost_hostId` FOREIGN KEY (`hostId`) REFERENCES `Hosts` (`hostId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_VerHost_versionId` FOREIGN KEY (`versionId`) REFERENCES `Versions` (`versionId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VerHost`
--

LOCK TABLES `VerHost` WRITE;
/*!40000 ALTER TABLE `VerHost` DISABLE KEYS */;
/*!40000 ALTER TABLE `VerHost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VerRepo`
--

DROP TABLE IF EXISTS `VerRepo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VerRepo` (
  `repoId` int(11) NOT NULL,
  `versionId` int(11) NOT NULL,
  `url` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`repoId`),
  KEY `fk_VerRepo_repoId_idx` (`repoId`),
  KEY `fk_VerRepo_versionId_idx` (`versionId`),
  CONSTRAINT `fk_VerRepo_repoId` FOREIGN KEY (`repoId`) REFERENCES `Repos` (`repoId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_VerRepo_versionId` FOREIGN KEY (`versionId`) REFERENCES `Versions` (`versionId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VerRepo`
--

LOCK TABLES `VerRepo` WRITE;
/*!40000 ALTER TABLE `VerRepo` DISABLE KEYS */;
/*!40000 ALTER TABLE `VerRepo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Versions`
--

DROP TABLE IF EXISTS `Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Versions` (
  `versionId` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(16) NOT NULL,
  `packetId` int(11) NOT NULL,
  PRIMARY KEY (`versionId`),
  UNIQUE KEY `versionId_UNIQUE` (`versionId`),
  KEY `fk_packetId_idx` (`packetId`),
  CONSTRAINT `fk_Versions_packetId` FOREIGN KEY (`packetId`) REFERENCES `Packets` (`packetId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Versions`
--

LOCK TABLES `Versions` WRITE;
/*!40000 ALTER TABLE `Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-18  0:03:14
