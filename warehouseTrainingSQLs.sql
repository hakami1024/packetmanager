SELECT * FROM Installation 
WHERE idOS IN (SELECT osId FROM OS WHERE osPlatform='linux');
  
SELECT COUNT(*), osPlatform 
FROM Installation 
INNER JOIN OS 
ON Installation.idOS = OS.osId
GROUP BY osPlatform;

SELECT login, osPlatform 
FROM Installation
INNER JOIN Users
ON Installation.idUser = Users.userId
INNER JOIN OS
ON Installation.idOS = OS.osId;